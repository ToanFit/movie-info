const imgUlr = 'http://image.tmdb.org/t/p/';
const apiKey = 'cea21eef9a7df7311969081a4e0846f4';
//search
$("form").submit(async function (event) {
    event.preventDefault();
    const strSearch = $('#searchBox input').val();
    loadingForSearch();
    const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&language=en-US&query=${strSearch}&page=1&include_adult=false`);
    const movies = await response.json();
    console.log(movies);
    $('#main').empty();
    for (const movie of movies.results) {
        $('#main').append(`
            <div class="col-md-4 py-2">
                <div class="card h-100" onclick="loadMovieDetail(${movie.id});
                                                 loadActor(${movie.id},0);
                                                 PaginationActor(${movie.id});
                                                 loadPreview(${movie.id})
                                                 ">
                    <img src="http://image.tmdb.org/t/p/w500/${movie.poster_path}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h2 class="card-text text-truncate">
                            ${movie.title}
                        </h2>
                        <p class="card-text text-truncate">${movie.overview}</p>
                    </div>
                </div>
            </div>
        `)
    }
});
//loading page
function loadingForSearch() {
    $('#main').empty();
    $('#main').append(`
        <div class="spinner-border m-5" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}
function loadingDetail() {
    $('#MainBody').empty();
    $('#MainBody').append(`
        <div class="spinner-border m-5" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}
function loadingActor() {
    $('#actor').empty();
    $('#actor').append(`
        <div class="spinner-border m-5" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}
function loadingPhim() {
    $('#phim').empty();
    $('#phim').append(`
        <div class="spinner-border m-5" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}
// load data
async function loadTrending(NumPage) {
    loadingForSearch();
    const response = await fetch(`https://api.themoviedb.org/3/trending/movie/week?api_key=${apiKey}&page=${NumPage}`);
    const movies = await response.json();
    $('#main').empty();
    for (var i = 0; i < 6; i++) {
        $('#main').append(`
            <div class="col-md-4 py-2">
                <div class="card h-100" onclick="loadMovieDetail(${movies.results[i].id});
                                                 loadActor(${movies.results[i].id},0);
                                                 PaginationActor(${movies.results[i].id});
                                                 loadPreview(${movies.results[i].id})
                                                 ">
                    <img src="http://image.tmdb.org/t/p/w500/${movies.results[i].poster_path}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h2 class="card-text text-truncate">
                            ${movies.results[i].title}
                        </h2>
                        <p class="card-text text-truncate">${movies.results[i].overview}</p>
                    </div>
                </div>
            </div>
        `)
    }
}
async function loadMovieDetail(movie_id) {
    //movie detail
    loadingDetail();
    const MovieResponse = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}?api_key=${apiKey}&language=en-US`);
    const detail = await MovieResponse.json();
    var QuocGia = "";
    for (var i = 0; i < detail.production_countries.length; i++) {
        QuocGia += detail.production_countries[i].name + ", ";
    }
    QuocGia = QuocGia.slice(0, QuocGia.length - 2);

    var TheLoai = "";
    for (var i = 0; i < detail.genres.length; i++) {
        TheLoai += detail.genres[i].name + ", ";
    }
    TheLoai = TheLoai.slice(0, TheLoai.length - 2);

    var company = "";
    for (var i = 0; i < detail.production_companies.length; i++) {
        company += detail.production_companies[i].name + ", ";
    }
    company = company.slice(0, company.length - 2);
    $('#MainBody').empty();
    $('#MainBody').append(`
        <h1 class="my-4">${detail.title}</h1> 
        <div class="row">
            <div class="col-md-5">
                <img class="img-fluid" src="http://image.tmdb.org/t/p/w500/${detail.backdrop_path}" alt="">
            </div>
            <div class="col-md-7">
                <h3 class="my-3">Nôi Dung Chính</h3>
                <p>${detail.overview}</p>
                <h3 class="my-3">Chi Tiết</h3>
                <ul>
                    <li>Phát hành bởi: ${company}</li>
                    <li>Quốc gia: ${QuocGia}</li>
                    <li>Năm Phát Hành: ${detail.release_date}</li>
                    <li>Thể Loại: ${TheLoai}</li>
                    <li>Thời Lượng: ${detail.runtime} phút</li>
                    <li>Điểm Bình Chọn: ${detail.vote_average}</li>
                </ul>
            </div>
        </div>
        <h3 class="my-4">Diễn Viên</h3>
        <div class="row" id="actor">
        </div>
        <nav aria-label="Page navigation example">
            <ul class="pagination" id="PActor">
                
            </ul>
        </nav>
        <h1>Previews</h1>
        <ul class="list-unstyled">
            <li class="media" >
                <div class="media-body">
                <h5 class="mt-0 mb-1">List-based media object</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
            </li>
        </ul>
    `)
}
async function loadActor(movie_id, NumPage) {
    loadingActor();
    const response = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=${apiKey}`);
    const actors = await response.json();
    const fake_img = 'http://placehold.it/500x750'
    console.log(actors);
    $('#actor').empty();
    for (var i = NumPage * 4; i < NumPage * 4 + 4; i++) {
        const img = actors.cast[i].profile_path ? `http://image.tmdb.org/t/p/w200/${actors.cast[i].profile_path}` : fake_img;
        $('#actor').append(`
        <div class="col-md-3 col-sm-6 mb-4" onclick = "loadActorDetail(${actors.cast[i].id});movieOfActor(${actors.cast[i].id},0);PaginationMovieActor(${actors.cast[i].id})">
            <img class="card-img" src=${img} alt="">
            <h4 class="text-center">${actors.cast[i].character}</h4>
            <p class="text-center">${actors.cast[i].name}</p>
        </div>
        `)
    }
}
async function loadActorDetail(actor_id) {
    loadingDetail();
    const response = await fetch(`https://api.themoviedb.org/3/person/${actor_id}?api_key=${apiKey}&language=en-US`);
    const detail = await response.json();
    const fake_img = 'http://placehold.it/500x750'
    var gioiTinh = detail.gender == 1 ? "female" : "male";
    $('#MainBody').empty();
    $('#MainBody').append(`
        <h1 class="my-4">${detail.name}</h1>
        <div class="row">

            <div class="col-md-4">
                <img class="img-fluid" src="http://image.tmdb.org/t/p/w300/${detail.profile_path}" alt="">
            </div>

            <div class="col-md-8">
                <h3 class="my-3">Tiểu Sử</h3>
                <p>${detail.biography}</p>
                <h3 class="my-3">Chi Tiết</h3>
                <ul>
                    <li>Nghề Nghiệp: ${detail.known_for_department}</li>
                    <li>giới tính: ${gioiTinh}</li>
                    <li>ngày sinh: ${detail.birthday}</li>
                    <li>Nơi Sinh: ${detail.place_of_birth}</li>
                </ul>
            </div>

        </div>
        <h3 class="my-4">Phim</h3>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tên Phim</th>
                    <th scope="col">Ngày Ra Mắt</th>
                    <th scope="col">Vai Diễn</th>
                    
                </tr>
            </thead>
            <tbody id="phim">
                
            </tbody>
        </table>
        <nav aria-label="Page navigation example">
            <ul class="pagination" id="PMovieActor">
                
            </ul>
        </nav>
    `)
}
async function movieOfActor(actor_id, NumPage) {
    loadingPhim();
    const response = await fetch(`https://api.themoviedb.org/3/person/${actor_id}/movie_credits?api_key=${apiKey}&language=en-US`);
    const movies = await response.json();
    $('#phim').empty();
    for (var i = NumPage * 6; i < NumPage * 6 + 6; i++) {
        $('#phim').append(`
            <tr onclick="loadMovieDetail(${movies.cast[i].id})">
                <td>
                    <img src="http://image.tmdb.org/t/p/w200/${movies.cast[i].poster_path}" alt="..." style="width: 50px;height: auto;">
                </td>
                <td>${movies.cast[i].title}</td>
                <td>${movies.cast[i].release_date}</td>
                <td>${movies.cast[i].character}</td>
            </tr>
        `)
    }
}
async function loadPreview(movie_id) {
    loadingActor();
    const response = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=${apiKey}&language=en-US`);
    const previews = await response.json();
    console.log(previews);
    $('.list-unstyled').empty();
    for (const preview of previews.results) {
        $('.list-unstyled').append(`
            <li class="media" style="border: 2px solid #001f3f; padding: 10px; border-radius: 10px; margin-bottom: 5px; ">
                <div class="media-body">
                <h5 class="mt-0 mb-1">${preview.author}</h5>
                ${preview.content}
                </div>
            </li>
        `)
    }
}
//pagination
async function PaginationTrending() {
    const response = await fetch(`https://api.themoviedb.org/3/trending/movie/week?api_key=${apiKey}`);
    const movies = await response.json();
    var NumPage = Math.ceil(movies.results.length / 3);
    $('#Ptrending').empty();
    for (var i = 1; i <= NumPage; i++) {
        $('#Ptrending').append(`
            <li class="page-item">
                <a class="page-link" href="#" onclick="loadTrending(${i})">${i}</a>
            </li>
        `)
    }

}
async function PaginationActor(movie_id) {
    const response = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=${apiKey}`);
    const actors = await response.json();
    var NumPage = Math.ceil(actors.cast.length / 4);
    $('#PActor').empty();
    for (var i = 0; i < NumPage; i++) {
        $('#PActor').append(`
            <li class="page-item">
                <a class="page-link" href="#" onclick="loadActor(${movie_id},${i})">${i + 1}</a>
            </li>
        `)
    }

}
async function PaginationMovieActor(actor_id) {
    const response = await fetch(`https://api.themoviedb.org/3/person/${actor_id}/movie_credits?api_key=${apiKey}&language=en-US`);
    const movies = await response.json();
    var NumPage = Math.ceil(movies.cast.length / 6);
    $('#PMovieActor').empty();
    for (var i = 0; i < NumPage; i++) {
        $('#PMovieActor').append(`
            <li class="page-item">
                <a class="page-link" href="#" onclick="movieOfActor(${actor_id},${i})">${i + 1}</a>
            </li>
        `)
    }

}
$().ready(() => {
    loadTrending(1);
    PaginationTrending();
})